package app;

public class CalcApp {
    // method for addition
    public double addValues(double val1, double val2){
        return val1 + val2;
    }

    // method for subtraction
    public double subValues(double val1, double val2){
        return val1 - val2;
    }

    // method for multiplication
    public double multiplyValues(double val1, double val2) {
        return val1 * val2;
    }

    // method for exponentiation
    public double powValues(double val1, double val2) {
        // throw exception if exponent if negative; it stops execution of the method
        if (val2 < 0) throw new ArithmeticException("Cannot calculate negative power.");
        // calculate result for val1^val2
        double res = 1;
        for(int i = 1; i <= val2; ++i){
            res *= val1;
        }
        return res;
    }

    // method for division
    public double divValues(double val1, double val2) {
        // throw exception if divider (val2) is zero; it stops execution of the method
        if (val2 == 0) throw new ArithmeticException("Cannot divide by 0.");
        // divide
        return val1 / val2;
    }

    // method for calculating the square root of a number
    public double sqrtValues(int val1) {
        // throw exception if number is zero or below; it stops execution of the method
        if (val1 <= 0) throw new ArithmeticException("Cannot find square root for 0 or negative value.");
        // calculate the square root; for simplification you can use Math.sqrt() instead of below code
        double tempVal;
        double sr = val1 / 2.0;
        do{
            tempVal = sr;
            sr = (sr + (val1 / sr)) / 2;
        } while((tempVal - sr) != 0);
        return sr;
    }
}