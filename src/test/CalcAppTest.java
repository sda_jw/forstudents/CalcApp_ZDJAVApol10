package test;

// import the class we will test
import app.CalcApp;

// import annotations from JUnit 5 Jupiter
import org.junit.jupiter.api.*;

// import assertions from JUnit 5 Jupiter
import static org.junit.jupiter.api.Assertions.*;
// import assertions from AssertJ
import static org.assertj.core.api.Assertions.*;

public class CalcAppTest{                                       // test class
    private CalcApp calc;                                       // test object

    @BeforeEach                                                 // annotation
    public void setUp() { calc = new CalcApp(); }               // the method will be executed before each test

    @Test                                                       // annotation
    @Tag("addValues")
    @Tag("TestSuite1")
    @DisplayName("MyFirstTest")                                 // this annotation changes name displayed in report
    public void testSumTwoPositives() {                         // this is test
        double expected = 8;
        double actual = calc.addValues(5, 3);                   // calling method of a test object that we are testing
        assertEquals(expected, actual);                         // assertion

        // sample usage of other assertions provided by JUnit
        // assertNotEquals; asserts two values are NOT equal
        assertNotEquals(9, actual);
        // assertTrue; asserts the parameter (value, condition) is true
        assertTrue(actual>5);
        // assertFalse; asserts the parameter (value, condition) is false
        assertFalse(actual<5);
        // assertNull; asserts the parameter is null
        CalcApp newCalc = null;
        assertNull(newCalc);
        // assertNotNull; asserts the parameter is NOT null
        assertNotNull(calc);
        // assertSame; asserts that parameters are referring to the same object
        CalcApp calc1 = new CalcApp();
        CalcApp calc2 = calc1;
        assertSame(calc1, calc2);
        // assertNotSame; asserts that parameters are NOT referring to the same object
        calc2 = new CalcApp();
        assertNotSame(calc1, calc2);
        // assertArrayEquals; asserts arrays are equal (the same length and same values in the same order)
        int[] arr1 = new int[]{1,2,3};
        int[] arr2 = new int[]{1,2,3};
        assertArrayEquals(arr1, arr2);
        // assertThrows; asserts that some code throws exception
        // first parameter is the type of exception, second is lambda expression in which you run your code to be tested
        // lambda: () -> { code }
        assertThrows(ArithmeticException.class, () -> {throw new ArithmeticException(); });
        // assertDoesNotThrow; asserts that some code does NOT throw exception
        // here only lambda expression
        assertDoesNotThrow(() -> {calc.addValues(2,3);});
        // fail; fails tests no matter what
        //fail();
    }

    // ###########
    // below tests are written for task from slideshow, placed at the end of JUnit part
    // ###########

    @Test
    // tags are used to run just selected group of tests
    // you can run them in SelectedTests class
    @Tag("subValues")
    @Tag("TestSuite1")
    public void testCheckIfSubNotEqualToFive(){
        double actual = calc.subValues(2, 3);
        assertNotEquals(5, actual);
    }

    @Test
    @Tag("subValues")
    @Tag("TestSuite2")
    public void testCheckIfSubNotNull(){
        double actual = calc.subValues(2, 3);
        assertNotNull(actual);
    }

    @Test
    @Tag("subValues")
    public void testCheckIfSubDoesNotThrowError(){
        assertDoesNotThrow(() -> {
            calc.subValues(2, 3);
        });
    }

    @Test
    // we can group many tags like this; just visual effect, doesn't affect functionality
    @Tags({@Tag("subValues"), @Tag("TestSuite2"), @Tag("Smoketests")})
    public void testCheckIfSubGreaterThanZero(){
        double actual = calc.subValues(8, 3);
        assertTrue(actual>0);
    }


    // ###########
    // AssertJ usage
    // ###########

    @Tags({@Tag("addValues"),@Tag("TestSuite2")})
    @DisplayName("MyFirstTestAssertJ")
    @Test
    public void testSumTwoPositivesAssertJ() {
        double expected = 8;
        double actual = calc.addValues(5, 3);
        assertEquals(expected, actual);
        assertThat(actual).isEqualTo(expected);
    }

    @Tag("addValues")
    @Test
    public void testMoreComplexAssertJ() {
        double expected = 8;
        double actual = calc.addValues(5, 3);
        assertEquals(expected, actual);
        assertThat(actual)
                .as("%f + %f = %f", 5, 3, expected)
                .isEqualTo(expected)
                .isNotEqualTo(11)
                .isGreaterThan(-999)
                .isLessThan(999)
                .isBetween(-999.0, 999.0)
                .isNotCloseTo(0, offset(0.1))
                .isNotNull()
                .isNotNaN()
                .isNotInstanceOf(Exception.class);
        assertThat(actual%2==0).isTrue();
    }

    // ###########
    // below tests are written for task 1 from slideshow, placed at the end of TDD part
    // ###########

    @Tags({@Tag("tdd"), @Tag("multiplyValues"), @Tag("Smoketests")})
    @Test
    public void testMultiplyTwoPositives(){
        double actual = calc.multiplyValues(2, 4);
        assertEquals(8, actual);
    }

    // this is first test written for powValues() method
    // i wrote this test, then wrote the method in CalcApp class, BUT
    // i wrote only part of the method necessary to pass the test (so there was no "if" statement and exception throwing)
    @Tags({@Tag("tdd"), @Tag("powValues"), @Tag("Smoketests")})
    @Test
    public void testValidPowerOfNumbers(){
        double actual = calc.powValues(4, 3);
        assertEquals(64, actual);
    }

    // this is second test written for powValues() method
    // i wrote the test and then the rest of the method ("if" and exception throwing)
    // after these two iteration of TDD, the functionality of powValues() was fully implemented
    @Tags({@Tag("tdd"), @Tag("powValues")})
    @Test
    public void testNotValidPowerOfNumbers(){
        assertThrows(ArithmeticException.class, () -> {calc.powValues(8, -10);});
    }

    // divValues() was implemented in similar way to powValues()
    @Tags({@Tag("tdd"), @Tag("divValues"), @Tag("Smoketests")})
    @Test
    public void testDivideTwoValidNumbers(){
        double actual = calc.divValues(8, 2);
        assertEquals(4, actual);
    }

    @Tags({@Tag("tdd"), @Tag("divValues")})
    @Test
    public void testDivideByZero(){
        assertThrows(ArithmeticException.class, () -> {calc.divValues(8, 0);});
    }

    // sqrtValues() was implemented in similar way to powValues()
    @Tags({@Tag("tdd"), @Tag("sqrtValues"), @Tag("Smoketests")})
    @Test
    public void testSROfValidNumber(){
        double actual = calc.sqrtValues(9);
        assertEquals(3, actual);
    }

    @Tags({@Tag("tdd"), @Tag("sqrtValues")})
    @Test
    public void testSROfInvalidNumber(){
        assertThrows(ArithmeticException.class, () -> {calc.sqrtValues(-9);});
    }
}