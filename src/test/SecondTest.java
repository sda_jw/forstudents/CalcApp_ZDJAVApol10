package test;

import app.CalcApp;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

// this class is here only to show how to run tests from two classes; check it in SelectedTests class
public class SecondTest {
    private CalcApp calc;

    @BeforeEach
    public void setUp() { calc = new CalcApp(); }

    @Tags({@Tag("addValues"), @Tag("TestSuite3"), @Tag("Smoketests")})
    @DisplayName("This test is in another class.")
    @Test
    public void testSumTwoPositives() {
        double expected = 8;
        double actual = calc.addValues(5, 3);
        assertEquals(expected, actual);
    }
}
